###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####################################################################
# File for running a Brunel initialisation test with default geometry
#####################################################################

from Configurables import Brunel
from Gaudi.Configuration import *

Brunel().EvtMax = 0
Brunel().DataType = "Upgrade"
Brunel().InputType = "MDF"
Brunel().Monitors = ["FPE", "SC"]
Brunel().DisableTiming = True

# Needed to switch off Lumi event handling and pick up SIMCOND while waiting for real MDF files!
Brunel().Simulation = True

# Database tags are temporary, this test should test the defaults  but currently default is HEAD and does not work
Brunel().CondDBtag = "upgrade/sim-20180530-vc-md100"
Brunel().DDDBtag = "upgrade/dddb-20170301"

# Switch off output
ApplicationMgr().OutStream = []
