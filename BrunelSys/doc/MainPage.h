/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor bruneldoxygenmain
 *
 * This reference manual documents all %LHCb and %Gaudi classes accessible from
 * the <a href="http://cern.ch/lhcbdoc/brunel/">Brunel reconstruction program</a> environment.<br>
 * More information on this and other %Brunel releases can be found in the
 * <a href="../index.php">Brunel project Web pages</a>
 *
 */
