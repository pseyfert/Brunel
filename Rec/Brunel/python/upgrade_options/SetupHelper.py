###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
  this file provides helper functions symplifying configuration of a Gaudi application
'''


def setupComponent(name,
                   instanceName=None,
                   packageName='Configurables',
                   **kwargs):
    imported = getattr(__import__(packageName, fromlist=[name]), name)
    if instanceName == None:
        instanceName = imported.getType()
    return imported(instanceName, **kwargs)


def _addIOVLockDependency(configurable):
    if hasattr(configurable, 'ExtraInputs'
               ) and '/Event/IOVLock' not in configurable.ExtraInputs:
        configurable.ExtraInputs.append('/Event/IOVLock')


def setupAlgorithm(name,
                   appMgr,
                   hiveDataBroker,
                   instanceName=None,
                   iovLockDependency=True,
                   **kwargs):
    # special handling for functors: in this case we are passed a python functor object and have to
    #                                unpack it into a few regular properties

    # import and create configurable
    configurable = setupComponent(name, instanceName=instanceName, **kwargs)
    # work  around limitations in IOVLock implementation
    if iovLockDependency:
        _addIOVLockDependency(configurable)
    # register it to the applicationManager and hiveDataBroker
    hiveDataBroker.DataProducers.append(configurable)
    appMgr.TopAlg.append(configurable)
    return configurable


def setupGaudiCore(nbEventSlots,
                   threadPoolSize,
                   evtMax,
                   debug=False,
                   timelineFile="",
                   dropEvtSel=False,
                   TESName="HiveWhiteBoard"):
    if timelineFile:
        setupComponent(
            "TimelineSvc",
            RecordTimeline=True,
            DumpTimeline=True,
            TimelineFile=timelineFile)

    whiteboard = setupComponent(
        TESName,
        instanceName="EventDataSvc",
        EventSlots=nbEventSlots,
        ForceLeaves=True)
    from Gaudi.Configuration import DEBUG
    eventloopmgr = setupComponent(
        'HLTControlFlowMgr',
        CompositeCFNodes=[('moore', 'LAZY_AND', [],
                           True)],  #sequence to be filled by defineSequence
        ThreadPoolSize=threadPoolSize)
    if dropEvtSel:
        appMgr = setupComponent(
            'ApplicationMgr',
            packageName='Gaudi.Configuration',
            EventLoop=eventloopmgr,
            EvtMax=evtMax,
            EvtSel='NONE')
    else:
        appMgr = setupComponent(
            'ApplicationMgr',
            packageName='Gaudi.Configuration',
            EventLoop=eventloopmgr,
            EvtMax=evtMax)
    appMgr.ExtSvc.insert(0, whiteboard)
    setupComponent('UpdateManagerSvc', WithoutBeginEvent=True)
    hiveDataBroker = setupComponent('HiveDataBrokerSvc')
    setupComponent('AlgContextSvc', BypassIncidents=True)  # for LoKi
    setupComponent('LoKiSvc', Welcome=False)

    if debug:
        eventloopmgr.OutputLevel = DEBUG
        hiveDataBroker.OutputLevel = DEBUG
    return appMgr, hiveDataBroker


def defineSequence(seq, appMgr):
    seqNames = []
    for s in seq:
        seqNames.append(s if isinstance(s, str) else s.name())
    appMgr.EventLoop.CompositeCFNodes[0][2].extend(seqNames)


def setupInput(inputFiles,
               dataType,
               DDDBTag,
               CONDDBTag,
               Simulation,
               inputFileType,
               outputFileType=None,
               withEventSelector=True):
    from GaudiConf import IOHelper
    iohelper = IOHelper(inputFileType, outputFileType)
    iohelper.setupServices()
    dddb = setupComponent('DDDBConf', Simulation=Simulation, DataType=dataType)
    conddb = setupComponent(
        'CondDB', Upgrade=True, Tags={
            "DDDB": DDDBTag,
            "SIMCOND": CONDDBTag
        })
    if (withEventSelector):
        evtSel = iohelper.inputFiles(inputFiles)
        inputs = []
        for inp in evtSel.Input:
            inputs.append(inp + " IgnoreChecksum='YES'")
        evtSel.Input = inputs
        evtSel.PrintFreq = 10000
        return inputs
    return inputFiles


def setupInputFromTestFileDB(testFileDBkey,
                             InputFiles=[],
                             withEventSelector=True):
    #if you want to have another file but use the testfileDB qualifiers, then set InputFiles
    from PRConfig.TestFileDB import test_file_db
    qualifiers = test_file_db[testFileDBkey].qualifiers
    dataType = qualifiers['DataType']
    Simulation = qualifiers['Simulation']
    fileType = "ROOT" if qualifiers['Format'] != "MDF" else qualifiers['Format']
    CondDBTag = qualifiers['CondDB']
    DDDBTag = qualifiers['DDDB']
    if not InputFiles:
        InputFiles = test_file_db[testFileDBkey].filenames
    return setupInput(InputFiles, dataType, DDDBTag, CondDBTag, Simulation,
                      fileType, None, withEventSelector)
