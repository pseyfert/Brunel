###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import Brunel, GaudiSequencer, CondDB, RootHistCnv__PersSvc, TimelineSvc
from PRConfig.TestFileDB import test_file_db
from GaudiConf import IOHelper
import os


def runTest(testFileDBkey,
            job=1,
            nJobs=1,
            evtMax=50,
            FTDecoVersion=2,
            filepaths=[]):

    brunel = Brunel()
    brunel.EvtMax = evtMax
    brunel.OutputType = "None"

    # Don't run "RecoPROTOSeq" for now
    brunel.RecoSequence = [
        "Decoding", "TrFast", "TrBest", "RICH", "CALOFUTURE", "MUON", "SUMMARY"
    ]

    #Hlt2 reconstruction sequence
    brunel.MainSequence = ['ProcessPhase/Reco']

    RootHistCnv__PersSvc().OutputEnabled = False
    from Configurables import FTRawBankDecoder
    FTRawBankDecoder("createFTClusters").DecodingVersion = FTDecoVersion

    #we will calculate the throuputs from the following csv files
    # if 'TAG' was not set (by the benchmark-scripts) we don't calculate the throughput (QMTest scenario)
    if 'TAG' in os.environ:
        if not os.path.exists('./time'):
            os.mkdir('./time')
        timeFileName = './time/Timeline_{:s}.{:s}.{:d}j.{:d}e.{:d}.csv'.format(
            os.environ['TAG'], os.environ['CMTCONFIG'], nJobs, evtMax, job)
        TimelineSvc(
            RecordTimeline=True, DumpTimeline=True, TimelineFile=timeFileName)

    from Configurables import TrackSys, L0Conf
    TrackSys().ExpertTracking = ["simplifiedGeometry"]
    L0Conf().EnsureKnownTCK = False

    # get input files from TestFileDB and split them for multiple jobs (we don't want to run the same events multiple times)
    # the splitting is the reason that this is a little longer than usual, and configuration has to be pulled from the TestFileDB dict
    qualifiers = test_file_db[testFileDBkey].qualifiers

    iohelper = IOHelper(qualifiers['Format'])
    iohelper.setupServices()

    brunel.Simulation = qualifiers['Simulation']
    brunel.DataType = qualifiers['DataType']
    brunel.DDDBtag = qualifiers['DDDB']
    brunel.CondDBtag = qualifiers['CondDB']
    if not filepaths:
        all_files = test_file_db[testFileDBkey].filenames
    else:
        all_files = filepaths

    print(all_files)
    if not all_files:
        raise RuntimeError(
            'List of input files is empty. If they are read from TestFileDB, make sure \"filenames\" and \"qualifiers\" are set properly.'
        )

    #split list of input files in equal sized chunks based on the number of jobs and select the chunk by the job number
    lower_index_as_fp = job * len(all_files) / float(nJobs)
    lower_index = int(lower_index_as_fp)
    upper_index_as_fp = (job + 1) * len(all_files) / float(nJobs)
    upper_index = int(upper_index_as_fp)
    if (upper_index - lower_index) > 1:
        upper_index -= 1  #don't run files twice
    elif (upper_index - lower_index) < 1:
        upper_index += 1  #we can't prevent running certain files multiple times in this case
    evtSel = iohelper.inputFiles(all_files[lower_index:upper_index])
    inputs = []
    for inp in evtSel.Input:
        inputs.append(inp + " IgnoreChecksum='YES'")
    evtSel.Input = inputs


if __name__ == "__builtin__":
    runTest("UpgradeHLT1FilteredMinbias", job=0, nJobs=1, evtMax=50)
