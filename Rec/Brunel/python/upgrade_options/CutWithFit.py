###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from upgrade_options.HLT1BaseLine import setupHLT1Reconstruction
from upgrade_options.SetupHelper import (
    setupGaudiCore,
    defineSequence,
)
from GaudiKernel.SystemOfUnits import mm, GeV


def runTest(testFileDBkey,
            nbEventSlots=1,
            threadPoolSize=1,
            evtMax=50000,
            FTDecoVersion=4,
            filepaths=[],
            checkEfficiency=False,
            FitterAlgo='ParameterizedKalman'):
    appMgr, hiveDataBroker = setupGaudiCore(
        nbEventSlots=nbEventSlots,
        threadPoolSize=threadPoolSize,
        evtMax=evtMax)
    GECCutVal = 11000
    if FTDecoVersion == 4:
        GECCutVal = 9750
    sequence = setupHLT1Reconstruction(
        appMgr,
        hiveDataBroker,
        testFileDBkey,
        filepaths,
        GECCut=GECCutVal,
        IPCut=True,
        IPCutVal=0.1 * mm,
        VeloMinPT=0.8 * GeV,
        FTMinPT=1.0 * GeV,
        FTDecoVersion=FTDecoVersion,
        checkEfficiency=checkEfficiency,
        FitterAlgo=FitterAlgo)

    defineSequence(sequence, appMgr)


if __name__ == "__builtin__":
    runTest(
        "MiniBrunel_2018_MinBias_FTv4_DIGI",
        nbEventSlots=1,
        threadPoolSize=1,
        evtMax=100,
        checkEfficiency=True)
