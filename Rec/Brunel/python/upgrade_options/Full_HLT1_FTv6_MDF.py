###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from upgrade_options.Full_HLT1 import runTest

if __name__ == "__builtin__":
    path = "/tmp/MiniBrunel_2019_MinBias_FTv6_MDF_small.mdf"
    import os
    if not os.path.isfile(path):
        os.system(
            "xrdcp -s root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/Beam7000GeV/Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up02/uncompressed_small.mdf %s"
            % path)
    files = [path] * 10
    runTest(
        "MiniBrunel_2019_MinBias_FTv6_MDF_small",
        nbEventSlots=6,
        threadPoolSize=5,
        evtMax=1000,
        UseIOSvc=True,
        filepaths=files,
        FTDecoVersion=6)
