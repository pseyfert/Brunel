###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import upgrade_options.CutWithFit


def runTest(testFileDBkey, **kwargs):
    kwargs.setdefault('FitterAlgo', 'VectorFitter')
    return upgrade_options.CutWithFit.runTest(testFileDBkey, **kwargs)


if __name__ == "__builtin__":
    runTest(
        "MiniBrunel_2018_MinBias_FTv4_DIGI",
        nbEventSlots=1,
        threadPoolSize=1,
        evtMax=100,
        checkEfficiency=True)
