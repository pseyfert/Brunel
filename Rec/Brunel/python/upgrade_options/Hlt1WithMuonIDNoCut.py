###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from upgrade_options.HLT1BaseLine import setupHLT1Reconstruction
from upgrade_options.SetupHelper import (
    setupGaudiCore,
    defineSequence,
)
from GaudiKernel.SystemOfUnits import mm, GeV


def runTest(
        testFileDBkey,
        nbEventSlots=1,
        threadPoolSize=1,
        evtMax=50000,
        FTDecoVersion=4,
        filepaths=[],
        checkEfficiency=False,
        # UseIOSvc is set to True for throughput tests, and overriden to False for nightlies (see below)
        UseIOSvc=True):
    GECCutVal = 11000
    if FTDecoVersion == 4:
        GECCutVal = 9750

    TESName = "EvtStoreSvc" if UseIOSvc else "HiveWhiteBoard"
    appMgr, hiveDataBroker = setupGaudiCore(
        nbEventSlots=nbEventSlots,
        threadPoolSize=threadPoolSize,
        evtMax=evtMax,
        dropEvtSel=UseIOSvc,
        TESName=TESName)
    sequence = setupHLT1Reconstruction(
        appMgr,
        hiveDataBroker,
        testFileDBkey,
        filepaths,
        GECCut=GECCutVal,
        TrackBeamLinePVs=True,
        FitterAlgo='VeloKalman',
        UseScifiTrackForwarding=True,
        FTDecoVersion=FTDecoVersion,
        checkEfficiency=checkEfficiency,
        UseVeloSIMD=True,
        UseIOSvc=UseIOSvc,
        WithMuonID=True)

    defineSequence(sequence, appMgr)


if __name__ == "__builtin__":
    runTest(
        "MiniBrunel_2018_MinBias_FTv4_DIGI",
        nbEventSlots=1,
        threadPoolSize=1,
        evtMax=1000,
        UseIOSvc=False,
        checkEfficiency=True)
