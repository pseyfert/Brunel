###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from upgrade_options.HLT1BaseLine import setupHLT1Reconstruction
from upgrade_options.SetupHelper import (
    setupGaudiCore,
    defineSequence,
)
from GaudiKernel.SystemOfUnits import mm, GeV


def runTest(testFileDBkey,
            nbEventSlots=1,
            threadPoolSize=1,
            evtMax=50000,
            FTDecoVersion=4,
            filepaths=[]):
    appMgr, hiveDataBroker = setupGaudiCore(
        nbEventSlots=nbEventSlots,
        threadPoolSize=threadPoolSize,
        evtMax=evtMax)
    GECCutVal = 11000
    if FTDecoVersion == 4:
        GECCutVal = 9750
    sequence = setupHLT1Reconstruction(
        appMgr,
        hiveDataBroker,
        testFileDBkey,
        filepaths,
        GECCut=GECCutVal,
        TrackBeamLinePVs=True,
        FitterAlgo=None,
        FTDecoVersion=FTDecoVersion,
        VeloModulesToSkip=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        UseFastForwardTracking=True)

    defineSequence(sequence, appMgr)


if __name__ == "__builtin__":
    runTest(
        "MiniBrunel_2018_MinBias_FTv4_MDF",
        nbEventSlots=6,
        threadPoolSize=5,
        evtMax=100)
