###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

__author__ = "Sebastien Ponce <Sebastien.Ponce@cern.ch>"

from Gaudi.Configuration import *
from Brunel.Configuration import Brunel
from Configurables import (
    LHCbConfigurableUser, LHCbApp, RecMoniConf, PrVeloUT,
    LHCb__DetDesc__ReserveDetDescForEvent as ReserveIOV,
    Gaudi__Hive__FetchDataFromFile as FetchDataFromFile,
    Rich__Future__RawBankDecoder as RichDecoder, TrackSys, HiveWhiteBoard,
    EventClockSvc, createODIN, LHCb__Tests__FakeEventTimeProducer as FET,
    TimelineSvc, CallgrindProfile,
    MeasurementProviderT_MeasurementProviderTypes__UTLite_,
    MeasurementProvider, PrPixelTracking)


def recurseAlgorithms(g, top):
    from GaudiConf.Manipulations import recurseConfigurables
    from Configurables import ProcessPhase

    def outer(i, f):
        recurseConfigurables(
            f,
            head=i,
            descend_properties=[
                'Members', 'Prescale', 'ODIN', 'L0DU', 'HLT', 'HLT1', 'HLT2',
                'Filter0', 'Filter1', 'Postscale'
            ])

    def inner(i):
        g(i)
        if isinstance(i, ProcessPhase):
            for det in i.DetectorList:
                outer(i.name() + det + "Seq", inner)

    outer(top, inner)


## @class MiniBrunel
#  configurable for the MiniBrunel application
#  @author Sebastien Ponce <sebastien.ponce@cern.ch>
class MiniBrunel(LHCbConfigurableUser):

    # Possible used configurables
    __used_configurables__ = [
        Brunel, LHCbApp, RecMoniConf, TrackSys, HiveWhiteBoard, EventClockSvc,
        createODIN, ReserveIOV, FetchDataFromFile, RichDecoder, FET
    ]

    # Steering options
    __slots__ = {
        "EvtMax": 1000,
        "IPCut": -1.0,
        "GECCut": -1,
        "minPTVeloUT": -1.0,
        "minPTForward": -1.0,
        "preselectionPTForward": -1.0,
        "forwardSecondLoop": True,
        "EnableHive": True,
        "LLAMode": "Disabled",
        "Scheduler": "HLTControlFlowMgr",
        "ThreadPoolSize": 2,
        "EventSlots": 2,
        "RunRich": True,
        "CreateTimeLine": True,
        "DisableTiming": True,
        "CallgrindProfile": False,
        "IntelProfile": False,
        "VeloOnly": False,
        "DecodingOnly": False,
        "HLT1Only": False,
        "HLT1BestPerf": False,
        "HLT1ParamKalman": False,
        "HLT1VectorKalman": False,
        "HLT1VectorMinimal": True,
        "HLT1Fitter": False,
        "RunFastForwardFitter": True,
        "RunChecks": False,
        "TimelineFile": "timeline.csv",
        "HistogramFile": "",
        "DDDBtag": "dddb-20171010",
        "CondDBtag": "sim-20170301-vc-md100",
        "IgnoreChecksum": False,
        "VeloClusteringVersion": "VPClus",
        "DataType": "Upgrade",
        "Simulation": False,
        "setControlFlowTreeMyself": False,
        "FTRawBankVersion": 2,
        "TopAlgs": [],
        "VeloStrategy": "ForwardThenBackward",
        "VeloConfiguration": "BestThroughput",
        "ExtremeScenario": False,
        "EventStoreType": "HiveWhiteBoard"
    }
    # Goes with the input file. For the moment I hardcode it here, but it needs to
    # move to a separate place such as the TestFileDB

    _propertyDocDct = {
        "EvtMax":
        """ Maximum number of events to process (default 1000) """,
        "GECCut":
        """ Value to be used to cut on FT+UT clusters""",
        "IPCut":
        """ Value to be used to cut on impact parameter""",
        "minPTVeloUT":
        """ Value to be used to set a minPT selection in VeloUT algorithm, for negative value just use default [GeV] units""",
        "minPTForward":
        """ Value to be used to set a minPT selection (for search windows) in Forward Tracking algorithm, for negative value just use default [GeV] units""",
        "preselectionPTForward":
        """ Value to be used to set a minPT selection (for search windows) in Forward Tracking algorithm, for negative value just use default [GeV] units""",
        "forwardSecondLoop":
        """ Enable/Disable for the HLT1 the Forward Tracking Second Loop """,
        "VeloStrategy":
        """ Reconstruction steps for the PixelTracking [OnlyForward, BackwardThenForward, ForwardThenBackward, OnlyBackward]""",
        "VeloConfiguration":
        """ Velo Configuration tuning for HLT1 [Default,BestPhysics, BestThroughput]""",
        "EnableHive":
        """ Whether to run in Hive mode (default true) """,
        "LLAMode":
        """ Mode for enabling alternative sequence that uses data processed by low-level accelarators (default is Disabled) """,
        "Scheduler":
        "Choose between AvalancheScheduler, HLTEventLoopMgr, HLTControlFlowMgr",
        "ThreadPoolSize":
        """ If EnableHive is set, size of the thread pool (default 2) """,
        "EventSlots":
        """ If EnableHive is set, number of event slot in the TES (default 2) """,
        "RunRich":
        """ Whether to run the Rich reconstruction (default yes) """,
        "CreateTimeLine":
        """ Whether to create a timeline csv (default yes) """,
        "DisableTiming":
        """ Disables the timing auditor if set (default is False) """,
        "CallgrindProfile":
        """ Enables callgrind wakeup instructions (default is False) """,
        "IntelProfile":
        """ Enables intel wakeup instructions (default is False) """,
        "HLT1Only":
        """ Runs MiniBrunel in HLT1 mode. All other algorithms are removes (including Rich) """,
        "HLT1BestPerf":
        """ Runs MiniBrunel with a set of configurations leading to optimal performance. to be used for benchmarks """,
        "VeloOnly":
        """ Runs MiniBrunel with Only PixelTracking in the sequence""",
        "DecodingOnly":
        """ Runs MiniBrunel with Only the Decodign in the sequence""",
        "HLT1Fitter":
        """ Use new HLT1Fitter instead of EventTrackFitter """,
        "HLT1ParamKalman":
        """ In case MiniBrunel is run in HLT1 mode, the ParameterizedKalman filter is used. """,
        "HLT1VectorKalman":
        """ In case MiniBrunel is run in HLT1 mode, the vectorized Kalman filter is used. """,
        "HLT1VectorMinimal":
        """ In case MiniBrunel is run in HLT1 mode and the vectorized Kalman filter is used, changes the setting for the vector fitter (default is True). """,
        "RunFastForwardFitter":
        """ When True, run RunFastForwardFitter, otherwise, drop it from the sequence (default is True) """,
        "RunChecks":
        """ whether to run MClinking and Prchecker (default is False) """,
        "TimelineFile":
        """ Name of the output file for timelines. (default is timeline.csv) """,
        "HistogramFile":
        """ Name of the output file for histograms. Overwrites the default when not empty (default is empty) """,
        "DDDBtag":
        """ Tag for DDDB (default is dddb-20170301) """,
        "CondDBtag":
        """ Tag for CondDB (default is sim-20170301-vc-md100) """,
        "IgnoreChecksum":
        """ Disable Checksum checks when reading MDF files """,
        "VeloClusteringVersion":
        """ Velo clustering to used [VPClus (default), VSPClus (faster)] """,
        "ExtremeScenario":
        """ Use extreme scenario configuration [ throw away modules in Velo up to module 13] """,
        "DataType":
        """ Data type, possible values defined in DDDBConf """,
        "Simulation":
        """ Flags whether to use SIMCOND conditions (default False) """,
        "FTRawBankVersion":
        """ FTRawBankVersion to use ( depends on the input sample ) """,
        "setControlFlowTreeMyself":
        """ if you want to define the entire control flow tree yourself rather than only defining top algs or letting minibrunel define,
                                         set this to true. Nothing will set HLTControlFlowMgr.CompositeCFNodes in this file """,
        "TopAlgs":
        """ if HLTControlFlowMgr is used as scheduler, you can define your TopAlgs yourself or let them be set automatically by other flags like HLT1Only.
                        Setting TopAlgs overwrites all changes to TopAlg by other flags if setControlFlowTreeMyself is False """,
    }

    KnownLLAModes = ["Disabled", "Clus"]

    def setFTRawBankVersion(self):
        from Configurables import FTRawBankDecoder
        if not FTRawBankDecoder("createFTClusters").isPropertySet(
                "DecodingVersion"):
            FTRawBankDecoder(
                "createFTClusters").DecodingVersion = self.getProp(
                    "FTRawBankVersion")
        else:
            log.warning(
                "FTRawBankDecoder('createFTClusters').DecodingVersion already set, ignoring miniBrunel().FTRawBankVersion"
            )

    # hack
    def co(self):
        app = ApplicationMgr()
        for c in allConfigurables:
            # until we have a proper implementation of the IOVLock dependency I need
            # a whitelist
            if c not in ('RichFutureDecode', 'ReserveIOV', 'createODIN',
                         'FetchDataFromFile'):
                c = allConfigurables[c]
                if hasattr(c, 'ExtraInputs'
                           ) and '/Event/IOVLock' not in c.ExtraInputs:
                    c.ExtraInputs.append('/Event/IOVLock')
        fetcher = FetchDataFromFile('FetchDataFromFile')
        # FIXME: this line can be removed once we find a way to collapse alternatives
        #        for unmet input dependencies
        fetcher.DataKeys = ['/Event/DAQ/RawEvent']
        if self.getProp("RunRich") and not self.getProp("HLT1Only"):
            richDecode = RichDecoder("RichFutureDecode")
            richDecode.ExtraInputs.append('/Event/IOVLock')
        seq = [fetcher, createODIN(), ReserveIOV('ReserveIOV')]
        if self.getProp("RunRich") and not self.getProp("HLT1Only"):
            seq.append(richDecode)
        # this is a hack because the raw file we are using does not have ODIN :(
        seq.insert(
            -1,
            FET("DummyEventTime",
                Start=EventClockSvc().InitialTime / 1E9,
                Step=0))
        j = 0
        for i in seq:
            app.TopAlg.insert(j, i)
            j = j + 1
        ReserveIOV('ReserveIOV').ODIN = FET(
            'DummyEventTime').ODIN = '/Event/DAQ/DummyODIN'

        # if we want to enable databroker also for the eventloopmgr!

        # if self.getProp("Scheduler") == "HLTEventLoopMgr" :
        #     from Configurables import HiveDataBrokerSvc
        #     from Configurables import HLTEventLoopMgr
        #     def accumulateDataProducers(i):
        #         if i.getType() not in [ "GaudiSequencer","ProcessPhase" ] and i not in HiveDataBrokerSvc().DataProducers:
        #             HiveDataBrokerSvc().DataProducers += [ i ]
        #     #  define the control flow nodes that we want to execute:
        #     if self.getProp("RunRich") and not self.getProp("HLT1Only"):
        #         for s in ["Long","Down","Up"]:
        #             HLTEventLoopMgr().TopAlg += [ "RichRecoStats"+s,"RiCKRes"+s,"RiCKRes"+s+"Tight","Ri"+s+"TrkEff" ]
        #         HLTEventLoopMgr().TopAlg += [ "RichRecPixelQC","RichDecodingErrors", "MergeRichPIDs" ]
        #     # find all data producers by brute force recursion...
        #     recurseAlgorithms(accumulateDataProducers, app.TopAlg)

    ## callgrind profiling
    def profile(self):
        if self.getProp("CallgrindProfile"):
            p = CallgrindProfile()
            p.StartFromEventN = 10
            p.StopAtEventN = self.getProp("EvtMax")
            p.DumpAtEventN = p.StopAtEventN
            ApplicationMgr().TopAlg.insert(0, p)
        if self.getProp("IntelProfile"):
            try:
                from Configurables import IntelProfile
            except ImportError:
                raise ImportError(
                    "Can't import IntelProfile configurable! Did you forget to setup the intel profiler before compiling?"
                )
            p = IntelProfile()
            p.StartFromEventN = 10
            p.StopAtEventN = self.getProp("EvtMax")
            ApplicationMgr().TopAlg.insert(0, p)

    ## cleanup for an HLT1 sequence

    def hlt1only(self):
        for a in list(AuditorSvc().Auditors):
            if a == 'ChronoAuditor' or (hasattr(a, 'name')
                                        and a.name() == 'ChronoAuditor'):
                AuditorSvc().Auditors.remove(a)
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(
                GaudiSequencer("TrackAddExtraInfoSeq"))
        except ValueError:
            None
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(
                GaudiSequencer("BestTrackCreatorSeq"))
        except ValueError:
            None
        try:
            newseq = GaudiSequencer("CheckPatSeq").Members[:5]
            GaudiSequencer("CheckPatSeq").Members = newseq[:]

        except ValueError:
            None
        if self.getProp("VeloOnly"):
            return

        if "vectorFitter" in TrackSys().getProp("ExpertTracking"):
            GaudiSequencer(
                "ForwardFitterAlgVectorFast"
            ).Fitter.MeasProvider.UTProvider = MeasurementProviderT_MeasurementProviderTypes__UTLite_(
            )
        elif not (
                self.getProp("HLT1Fitter") or
            ("ParameterizedKalman" in TrackSys().getProp("ExpertTracking"))):
            GaudiSequencer(
                "ForwardFitterAlgFast"
            ).Fitter.MeasProvider.UTProvider = MeasurementProviderT_MeasurementProviderTypes__UTLite_(
            )

    ## cleanup for an HLT1 sequence
    def donotrunfastforwardfitter(self):
        from Configurables import PrTrackAssociator
        try:
            GaudiSequencer("RecoTrFastSeq").Members.remove(
                GaudiSequencer("ForwardFitterAlgFast"))
            GaudiSequencer("ForwardFastFittedExtraChecker").Members.remove(
                PrTrackAssociator("ForwardFastFittedAssociator"))
        except ValueError:
            None

    def decodingonly(self):
        GaudiSequencer("RecoTrFastSeq").Members = []

    ## put everything in one sequence so PrGECFilter works
    def fixGECForBenchmark(self):
        if self.getProp("HLT1Only"):
            try:
                FastSeq = GaudiSequencer("RecoTrFastSeq")
                Parallel = GaudiSequencer("ParallelSeq")
                Filter = GaudiSequencer(
                    "FilterSeq",
                    ModeOR=False,
                    ShortCircuit=True,
                    Sequential=True)
                PrGECFilter = GaudiSequencer("RecoDecodingSeq").Members.pop(0)
                PrGECFilter.NumberFTUTClusters = self.getProp("GECCut")
                if not self.getProp("VeloOnly"):
                    PrStoreFT = GaudiSequencer("RecoDecodingSeq").Members.pop(
                        -1)
                    PrStoreUT = GaudiSequencer("RecoDecodingSeq").Members.pop(
                        -1)
                    CreateFTClusters = GaudiSequencer(
                        "RecoDecodingSeq").Members.pop(-1)
                    Parallel.Members = [CreateFTClusters, PrStoreFT, PrStoreUT]

                Parallel.Members.extend(FastSeq.Members)
                Filter.Members = [PrGECFilter, Parallel]
                GaudiSequencer("RecoDecodingSeq").Members.append(Filter)
                FastSeq.Members = []
            except ValueError:
                log.error("GECForBenchmarking HACK failed, fix this")
                raise

    def llaClus(self):
        from Configurables import VPClus, VPRetinaClusterDecoder
        members = GaudiSequencer("ParallelSeq").Members
        alg_tobereplaced = VPClus("VPClustering")
        members.insert(
            members.index(alg_tobereplaced),
            VPRetinaClusterDecoder("RetinaClusterDecoder"))
        members.remove(alg_tobereplaced)

    def disableChecksum(self):
        from GaudiConf import IOExtension
        evtSelector = IOExtension().inputFiles([])
        inputs = []
        for inp in evtSelector.Input:
            inputs.append(inp + " IgnoreChecksum='YES'")
        evtSelector.Input = inputs

    def ControlFlowConfig(self):
        from Configurables import HiveDataBrokerSvc
        from Configurables import HLTControlFlowMgr, TrackBestTrackCreator

        def accumulateDataProducers(i):
            if i.getType() not in [
                    "GaudiSequencer", "ProcessPhase"
            ] and i not in HiveDataBrokerSvc().DataProducers:
                HiveDataBrokerSvc().DataProducers += [i]

        # find all data producers by brute force recursion...
        recurseAlgorithms(accumulateDataProducers, ApplicationMgr().TopAlg)
        if self.getProp("CallgrindProfile"):
            from Configurables import CallgrindProfile
            HLTControlFlowMgr().AdditionalAlgs = [CallgrindProfile().name()]
        elif self.getProp("IntelProfile"):
            from Configurables import IntelProfile
            HLTControlFlowMgr().AdditionalAlgs = [IntelProfile().name()]

        if not self.getProp("setControlFlowTreeMyself"):  # or set it yourself!
            TopAlgorithms = self.getProp("TopAlgs")
            #define the topalgs according to the chosen sequence
            if TopAlgorithms == []:
                TopAlgorithms += ["createODIN"]
                if self.getProp("GECCut") > 0:
                    TopAlgorithms += ["PrGECFilter"]
                TopAlgorithms += ["PatPV3D"]
                if self.getProp("HLT1Only") or not self.getProp("RunRich"):
                    if self.getProp("HLT1ParamKalman"):
                        TopAlgorithms += ["ForwardFitterAlgParamFast"]
                    elif self.getProp("HLT1VectorKalman"):
                        TopAlgorithms += ["ForwardFitterAlgVectorFast"]
                    elif self.getProp("RunFastForwardFitter"):
                        TopAlgorithms += ["ForwardFitterAlgFast"]
                    else:
                        TopAlgorithms += ["PrForwardTrackingFast"]
                    if self.getProp(
                            "VeloOnly"
                    ):  #mimicks behaviour of eventloopmgr with VeloOnly set to True
                        TopAlgorithms = ["createODIN", "ReserveIOV"]
                else:
                    # This replaces TrackAddNNGhostId algorithm that should not be used in threaded context
                    # it however is in the list of algos due to the reuse of hte old sequence but won't be
                    # triggered. Instead we activate the ghost probability computation in TrackBestTrackCreator
                    TrackBestTrackCreator(
                        "TrackBestTrackCreator").AddGhostProb = True
                    for s in ["Long", "Down", "Up"]:
                        TopAlgorithms += [
                            "RichRecoStats" + s, "RiCKRes" + s,
                            "RiCKRes" + s + "Tight", "Ri" + s + "TrkEff"
                        ]
                    TopAlgorithms += [
                        "RichRecPixelQC", "RichRecPixelClusters",
                        "RichDecodingErrors", "MergeRichPIDs",
                        "FutureEcalCovar"
                    ]

            #this is the important option to define the controlflowmgr
            HLTControlFlowMgr().CompositeCFNodes = [('MiniBrunel', 'LAZY_AND',
                                                     TopAlgorithms, True)]

    ## Apply the configuration
    def __apply_configuration__(self):
        brunel = Brunel()
        brunel.DataType = self.getProp("DataType")
        brunel.EvtMax = self.getProp("EvtMax")
        brunel.DatasetName = "future"
        brunel.InitSequence = []
        if not self.getProp("RunRich") or self.getProp("HLT1Only"):
            brunel.MainSequence = ["ProcessPhase/Reco"]
            brunel.RecoSequence = ["Decoding", "TrFast"]
            TrackSys().TracksToConvert = ["Velo", "Upstream", "ForwardFast"]
            if self.getProp("VeloOnly") and self.getProp("RunChecks"):
                TrackSys().TracksToConvert = ["Velo"]
        else:
            brunel.MainSequence = ["ProcessPhase/Reco", "ProcessPhase/Moni"]
            brunel.RecoSequence = [
                "Decoding", "TrFast", "TrBest", "RICH", "CALOFUTURE"
            ]
            RecMoniConf().MoniSequence = ["RICH"]
        if self.getProp("RunChecks"):
            brunel.MainSequence.append('ProcessPhase/MCLinks')
            brunel.MainSequence.append('ProcessPhase/Check')
            brunel.WithMC = True
            if not brunel.isPropertySet("MCLinksSequence"):
                brunel.MCLinksSequence = ["Unpack", "Tr"]
        else:
            brunel.WithMC = False
        if self.getProp("VeloOnly"):
            brunel.Detectors = ["VP"]
        else:
            brunel.Detectors = ["VP", "UT", "FT"]
        brunel.OutputType = "NONE"
        brunel.PackType = "NONE"
        brunel.Simulation = self.getProp("Simulation")
        brunel.InputType = 'DIGI'
        brunel.DDDBtag = self.getProp("DDDBtag")
        brunel.CondDBtag = self.getProp("CondDBtag")
        brunel.DisableTiming = self.getProp("DisableTiming")

        TrackSys().TrackingSequence = ["TrFast", "TrBest"]
        if self.getProp("HLT1Only"):
            TrackSys().TrackingSequence = ["TrFast"]
        if self.getProp("HLT1Fitter"):
            TrackSys().TrackingSequence.append("TrHlt")
        TrackSys().TrackTypes = [
            "Velo", "Upstream", "Forward", "Downstream", "Seeding", "Match"
        ]
        if self.getProp("VeloOnly"):
            TrackSys().TrackTypes = ["Velo"]
        TrackSys().ExpertTracking = ["simplifiedGeometry"]
        if self.getProp("HLT1ParamKalman"):
            TrackSys().ExpertTracking.append("ParameterizedKalman")
        elif self.getProp("HLT1VectorKalman"):
            TrackSys().ExpertTracking.append("vectorFitter")
            if self.getProp("HLT1VectorMinimal"):
                TrackSys().ExpertTracking.append("minimalVectorFitterFast")

        TrackSys().ConfigHLT1["VeloHLT1"]["Strategy"] = self.getProp(
            "VeloStrategy")
        TrackSys().ConfigHLT1["VeloHLT1"]["Configuration"] = self.getProp(
            "VeloConfiguration")
        if self.getProp("HLT1BestPerf"):
            TrackSys().BestThroughput = True

        if self.getProp("ExtremeScenario"):
            TrackSys().ConfigHLT1["ExtremeScenario"] = True
            TrackSys().ConfigHLT1["VeloHLT1"]["Strategy"] = "OnlyForward"
        TrackSys().VeloClustering = self.getProp("VeloClusteringVersion")
        LHCbApp().EnableHive = self.getProp("EnableHive")
        if LHCbApp().EnableHive:
            if self.getProp("EventStoreType") == "EvtStoreSvc":
                whiteboard = EvtStoreSvc("EventDataSvc")
            else:
                whiteboard = HiveWhiteBoard("EventDataSvc")
            whiteboard.EventSlots = self.getProp("EventSlots")

        LHCbApp().Scheduler = self.getProp("Scheduler")
        LHCbApp().ThreadPoolSize = self.getProp("ThreadPoolSize")
        EventClockSvc(InitialTime=1433509200000000000)
        if self.getProp("VeloOnly"):
            TrackSys().VeloUpgradeOnly = True

        if LHCbApp().EnableHive:
            appendPostConfigAction(self.co)
        if self.getProp("CallgrindProfile") or self.getProp("IntelProfile"):
            appendPostConfigAction(self.profile)
        if self.getProp("HLT1Only"):
            appendPostConfigAction(self.hlt1only)
        if not self.getProp("RunFastForwardFitter"):
            appendPostConfigAction(self.donotrunfastforwardfitter)
        #Set IP cut , minPT VeloUT and minPT forward overloading defaults from TrackSys or the ones set by hlt1BestPerf
        if self.getProp("IPCut") > 0:
            from GaudiKernel.SystemOfUnits import mm
            TrackSys().ConfigHLT1["VeloUTHLT1"]["IPCut"] = True
            TrackSys(
            ).ConfigHLT1["VeloUTHLT1"]["IPCutVal"] = self.getProp("IPCut") * mm
        else:
            TrackSys().ConfigHLT1["VeloUTHLT1"]["IPCut"] = False

        from GaudiKernel.SystemOfUnits import GeV

        if self.getProp("minPTVeloUT") > 0:
            #Configure the minPT of the VeloUT if the value passed is > 0
            TrackSys().ConfigHLT1["VeloUTHLT1"]["minPT"] = self.getProp(
                "minPTVeloUT") * GeV
        if self.getProp("minPTForward") > 0:
            #Configure the minPT of the Forward if the value passed is > 0
            TrackSys().ConfigHLT1["ForwardHLT1"]["minPT"] = self.getProp(
                "minPTForward") * GeV
        if self.getProp("preselectionPTForward") > 0:
            #Configure the preselectionPT for the Forward HLT1 tracking
            TrackSys().ConfigHLT1["ForwardHLT1"][
                "PreselectionPT"] = self.getProp("preselectionPTForward") * GeV

        TrackSys().ConfigHLT1["ForwardHLT1"]["SecondLoop"] = self.getProp(
            "forwardSecondLoop")
        if self.getProp("GECCut") > 0:
            TrackSys().ConfigHLT1["GEC"] = self.getProp("GECCut")

        appendPostConfigAction(self.setFTRawBankVersion)

        # This allows correct use of the GEC filter, but only for now.
        #FIXME The real fix is to rework how we define the tracking sequence
        if self.getProp("GECCut") > 0:
            appendPostConfigAction(self.fixGECForBenchmark)
        if self.getProp("DecodingOnly"):
            appendPostConfigAction(self.decodingonly)

        #LLA modes
        llamode = self.getProp("LLAMode")
        print llamode
        if llamode in self.KnownLLAModes:
            if llamode == "Clus":
                appendPostConfigAction(self.llaClus)
        else:
            raise RuntimeError("Invalid mode for LLA: '%s'" % llamode)

        if self.getProp("CreateTimeLine"):
            TimelineSvc(
                RecordTimeline=True,
                DumpTimeline=True,
                TimelineFile=self.getProp("TimelineFile"))
        if self.getProp("HistogramFile"):
            HistogramPersistencySvc().OutputFile = self.getProp(
                "HistogramFile")
        if self.getProp("IgnoreChecksum"):
            self.disableChecksum()

        if self.getProp("Scheduler") == "HLTControlFlowMgr":
            appendPostConfigAction(self.ControlFlowConfig)
