###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

from Configurables import MiniBrunel, EventSelector, TrackListRefiner, DeterministicPrescaler, PrCheckEmptyTracks
from Configurables import LoKi__Hybrid__TrackSelector as LoKiTrackSelector
from Configurables import HLTControlFlowMgr, HLTEventLoopMgr, HiveDataBrokerSvc

mbrunel = MiniBrunel()
mbrunel.Scheduler = "HLTControlFlowMgr"
mbrunel.GECCut = 9750
mbrunel.EnableHive = True
mbrunel.EvtMax = 100
mbrunel.ThreadPoolSize = 10
mbrunel.EventSlots = 12
mbrunel.DisableTiming = False
mbrunel.HLT1Only = True

#controlflow conf
HLTControlFlowMgr().PrintFreq = 10
HLTControlFlowMgr().OutputLevel = DEBUG
HiveDataBrokerSvc().OutputLevel = DEBUG

d1 = DeterministicPrescaler('detPrescaler1', AcceptFraction=.9)
d2 = DeterministicPrescaler('detPrescaler2', AcceptFraction=.8)

Selection1 = TrackListRefiner(
    'Selection1',
    inputLocation='Rec/Track/ForwardFastFitted',
    outputLocation='Rec/Track/ForwardFastFittedSelected1')
Selection1.addTool(LoKiTrackSelector, name="SomeSelector1")
Selection1.Selector = LoKiTrackSelector(name="SomeSelector1")
Selection1.Selector.Code = "TrPT>{}".format(1000 * MeV)
Selection1.Selector.StatPrint = True

Selection2 = TrackListRefiner(
    'Selection2',
    inputLocation='Rec/Track/ForwardFastFitted',
    outputLocation='Rec/Track/ForwardFastFittedSelected2')
Selection2.addTool(LoKiTrackSelector, name="SomeSelector2")
Selection2.Selector = LoKiTrackSelector(name="SomeSelector2")
Selection2.Selector.Code = "TrPT<{}".format(25000 * MeV)
Selection2.Selector.StatPrint = True

CEFP1 = PrCheckEmptyTracks(
    'CEF1', inputLocation='Rec/Track/ForwardFastFittedSelected1')
CEFP2 = PrCheckEmptyTracks(
    'CEF2', inputLocation='Rec/Track/ForwardFastFittedSelected2')

line1_children = ['detPrescaler1', 'PrGECFilter', 'PatPV3D', 'CEF1']
line2_children = ['detPrescaler2', 'PrGECFilter', 'PatPV3D', 'CEF2']

mbrunel.setControlFlowTreeMyself = True
HLTControlFlowMgr().CompositeCFNodes = [
    ('MiniMoore', 'NONLAZY_AND', ['line1', 'line2'], False),
    ('line1', 'LAZY_AND', line1_children, True),
    ('line2', 'LAZY_AND', line2_children, True),
]

filedb = test_file_db['MiniBrunel_2018_MinBias_FTv2_MDF']
filedb.run(configurable=mbrunel)

#this has to be done right now, will not be needed in the future (hopefully)
ApplicationMgr().TopAlg += [d1, d2, Selection1, Selection2, CEFP1, CEFP2]
