###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import MiniBrunel, EventSelector
from Gaudi.Configuration import *
from Configurables import HLTControlFlowMgr, HLTEventLoopMgr, HiveDataBrokerSvc
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

#need to do that
# from Configurables import FTRawBankDecoder
# FTRawBankDecoder("createFTClusters").DecodingVersion = 2

mbrunel = MiniBrunel()
mbrunel.Scheduler = "HLTControlFlowMgr"
# mbrunel.Scheduler = "HLTEventLoopMgr"
# mbrunel.GECCut = 11000
mbrunel.EnableHive = True
mbrunel.EvtMax = 100
mbrunel.ThreadPoolSize = 4
mbrunel.EventSlots = 4
mbrunel.DisableTiming = False
# mbrunel.CallgrindProfile = True
# mbrunel.HLT1Only = True
# mbrunel.HLT1BestPerf = True
# mbrunel.VeloOnly = True
# mbrunel.RunRich = True
# mbrunel.IgnoreChecksum = True

#controlflow conf
HLTControlFlowMgr().PrintFreq = 1
HLTControlFlowMgr().OutputLevel = DEBUG
HLTEventLoopMgr().OutputLevel = DEBUG
HiveDataBrokerSvc().OutputLevel = DEBUG

# mbrunel.setControlFlowTreeMyself = True
# if you want to write CF yourself:
# HLTControlFlowMgr().CompositeCFNodes = [
#   ( 'MiniBrunel', 'NONLAZY_OR', ['line1', 'line2'], False ),
#   ( 'line1', 'LAZY_AND', ['PrGECFilter', 'SomeSelection], True ),
#   ( 'line2', 'LAZY_AND', ['PrGECFilter', 'SomeOtherSelection'], True ),
# ]

# HLTEventLoopMgr( TopAlg = topalgs)
# overwrite files from the filedb
filedb = test_file_db['MiniBrunel_2018_MinBias_FTv2_MDF']
# filedb.filenames = ['/localdisk1/hlt1/chasse/UpgradeMC10_2017/MD/MC_Upg_Down_201710_43k.mdf']
filedb.run(configurable=mbrunel)
# files = [ '/localdisk1/hlt1/chasse/UpgradeMC10_2017/MD/MC_Upg_Down_201710_43k.mdf']
# IOHelper("MDF").inputFiles(files)
