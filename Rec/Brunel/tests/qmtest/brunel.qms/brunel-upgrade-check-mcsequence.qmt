<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
#######################################################
# SUMMARY OF THIS TEST
# ...................
# Author: M.Cattaneo
# Purpose: Look for any changes in the sequence of algorithms called in the Upgrade configuration of Brunel, with MC checking enabled
#          Test fails if there is any change in sequence of algorithms called, both during initialisation/finalisation and event loop.
#          Insensitive to change in output other than that of the NameAuditor
#######################################################
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>gaudirun.py</text></argument>
  <argument name="timeout"><integer>2400</integer></argument>
  <argument name="args"><set>
    <text>$BRUNELROOT/tests/options/testBrunel-defaults.py</text>
    <text>$APPCONFIGOPTS/Brunel/MC-WithTruth.py</text>
  </set></argument>
  <argument name="options"><text>
from Configurables import Brunel
Brunel().DataType = "Upgrade"
Brunel().EvtMax=1
Brunel().Monitors+=["NameAuditor"]
from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 2
from PRConfig import TestFileDB 
TestFileDB.test_file_db['upgrade-baseline-FT61-digi'].run()
</text></argument>
  <argument name="reference"><text>../refs/brunel-upgrade-check-mcsequence.ref</text></argument>
  <argument name="validator"><text>
preprocessor = normalizeExamples + \
               LineSkipper( regexps = [ "^((?!NameAuditor).)*$" ] )
counter_preprocessor = LineSkipper( regexps = [ ".*" ] )
validateWithReference(preproc = preprocessor, counter_preproc=counter_preprocessor)
</text></argument>
</extension>
