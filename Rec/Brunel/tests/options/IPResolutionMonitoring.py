###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import CondDB, LHCbApp, Brunel, LHCbConfigurableUser, LHCbApp, L0Conf, TrackSys, DigiConf, FTRawBankDecoder
from Brunel.Configuration import Brunel
from GaudiConf import IOHelper

Evts_to_Run = 100

myBrunel = Brunel(
    DataType='Upgrade',
    EvtMax=Evts_to_Run,
    PrintFreq=50,
    WithMC=True,
    Simulation=True,
    OutputType='None',
    Detectors=['VP', 'UT', 'Magnet', 'FT', 'Tr'],
    InputType='DIGI')


def addMonitor():
    from Configurables import TrackIPResolutionCheckerNT
    from Configurables import TrackSys

    ip_resolution_monitors = []

    for track_type in ["ForwardBest", "ForwardFastFitted"]:
        ip_resolution_checkerNT = TrackIPResolutionCheckerNT(
            "ipUsingTruth{}".format(track_type))
        ip_resolution_checkerNT.TrackContainer = TrackSys(
        ).DefaultConvertedTrackLocations[track_type]["Location"]
        ip_resolution_checkerNT.LinkerLocation = "Link/" + ip_resolution_checkerNT.TrackContainer
        ip_resolution_monitors += [ip_resolution_checkerNT]

    GaudiSequencer('RecoPROTOSeq').Members = []
    GaudiSequencer('RecoRICHSeq').Members = []
    GaudiSequencer("RecoCALOFUTURESeq").Members = []
    GaudiSequencer("RecoMUONSeq").Members = []
    GaudiSequencer("MatchChecker").Members = []
    GaudiSequencer("DownstreamChecker").Members = []
    GaudiSequencer("UpstreamChecker").Members = []
    GaudiSequencer("SeedingChecker").Members = []
    GaudiSequencer("BestExtraChecker").Members = []
    GaudiSequencer("RecSUMMARYSeq").Members = []
    GaudiSequencer("MoniTrSeq").Members = []
    GaudiSequencer("MoniPROTOSeq").Members = []
    GaudiSequencer("CheckPatSeq").Members = ip_resolution_monitors


appendPostConfigAction(addMonitor)

from Configurables import NTupleSvc
NTupleSvc().Output += ["FILE1 DATAFILE='tuples.root' TYPE='ROOT' OPT='NEW'"]

DigiConf().EnableUnpack = True
L0Conf.EnsureKnownTKC = False

FTRawBankDecoder("createFTClusters").DecodingVersion = 2

from PRConfig.TestFileDB import test_file_db
test_file_db['upgrade-prchecker-digi'].run(configurable=myBrunel)
