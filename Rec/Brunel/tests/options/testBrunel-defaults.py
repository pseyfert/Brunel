###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Default settings for all Brunel tests
from Brunel.Configuration import *

Brunel().Monitors = ["SC", "FPE"]  # Add StatusCode checks
Brunel().DisableTiming = True

# Disable known FPEs...
#from Configurables import FPEAuditor
#FPEAuditor().DisableTrapFor += ['MergedPi0Rec']

# Use current time in initialisation
#from datetime import datetime
#from Configurables import EventClockSvc
#ecs = EventClockSvc()
#dt = datetime.utcnow() - datetime(1970, 1, 1, 0)
#ns = (dt.days * 24 * 60 * 60 + dt.seconds) * 1000000000
#ecs.InitialTime = ns
