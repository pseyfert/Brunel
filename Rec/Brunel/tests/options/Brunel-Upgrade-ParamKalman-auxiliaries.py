###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel, CondDB, LHCbApp, DDDBConf
Brunel().DataType = "Upgrade"
Brunel().OutputType = "NONE"
Brunel().Simulation = True
Brunel().DisableTiming = True
Brunel().RecoSequence = ["Decoding", "TrFast"]
Brunel().EvtMax = 1
Brunel().Detectors = ["VP", "UT", "FT"]

from Configurables import RecMoniConf
RecMoniConf().MoniSequence = []

from Configurables import TrackSys
TrackSys().TrackingSequence = ["TrFast"]
TrackSys().ExpertTracking = ["simplifiedGeometry"]

from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 2

from Configurables import MCParticle2MCHitAlg
FTAssoc = MCParticle2MCHitAlg(
    "MCP2FTMCHitAlg",
    MCHitPath="MC/FT/Hits",
    OutputData="/Event/MC/Particles2MCFTHits")
VPAssoc = MCParticle2MCHitAlg(
    "MCP2VPMCHitAlg",
    MCHitPath="MC/VP/Hits",
    OutputData="/Event/MC/Particles2MCVPHits")
UTAssoc = MCParticle2MCHitAlg(
    "MCP2UTMCHitAlg",
    MCHitPath="MC/UT/Hits",
    OutputData="/Event/MC/Particles2MCUTHits")

from Configurables import DataOnDemandSvc
DataOnDemandSvc().AlgMap["/Event/Link/MC/Particles2MCFTHits"] = FTAssoc
DataOnDemandSvc().AlgMap["/Event/Link/MC/Particles2MCVPHits"] = VPAssoc
DataOnDemandSvc().AlgMap["/Event/Link/MC/Particles2MCUTHits"] = UTAssoc
DataOnDemandSvc().NodeMap["/Event/Link"] = "DataObject"
DataOnDemandSvc().NodeMap["/Event/Link/MC"] = "DataObject"

from PRConfig.TestFileDB import test_file_db
test_file_db["upgrade_2018_BsPhiPhi_FTv2"].run()

from Gaudi.Configuration import *


def doIt():
    ### Run Tuner
    from Configurables import IdealStateCreator
    from Configurables import ParameterizedKalmanFit_Checker, TrackMasterExtrapolator
    ParameterizedKalmanC = ParameterizedKalmanFit_Checker()
    ParameterizedKalmanC.RunForTuning = False
    ParameterizedKalmanC.MaxNumOutlier = 0
    ParameterizedKalmanC.InputName = "Rec/Track/Keyed/ForwardFast"
    ParameterizedKalmanC.OutputName = "Rec/Track/Keyed/ForwardFastFitted_Param"
    ParameterizedKalmanC.LinkerLocation = "Link/Rec/Track/Keyed/ForwardFast"

    ParameterizedKalmanC.OutputTreesFile = "TMP"

    ParameterizedKalmanC.addTool(TrackMasterExtrapolator, name="extr1")
    ParameterizedKalmanC.addTool(TrackMasterExtrapolator, name="extr2")
    ParameterizedKalmanC.extr1.ApplyMultScattCorr = True
    ParameterizedKalmanC.extr1.ApplyEnergyLossCorr = True
    ParameterizedKalmanC.extr1.ApplyElectronEnergyLossCorr = True
    ParameterizedKalmanC.extr2.ApplyMultScattCorr = True
    ParameterizedKalmanC.extr2.ApplyEnergyLossCorr = False
    ParameterizedKalmanC.extr2.ApplyElectronEnergyLossCorr = True

    ParameterizedKalmanC.addTool(IdealStateCreator("IdealStateCreator"))
    ParameterizedKalmanC.IdealStateCreator.Extrapolator = "TrackMasterExtrapolator"
    ParameterizedKalmanC.IdealStateCreator.Detectors = ["VP", "UT", "FT"]

    from Configurables import CompareTracksTrTr, IdealStateCreator
    CompTracks = CompareTracksTrTr()

    CompTracks.InputTracks1 = "Rec/Track/ForwardFastFitted"
    CompTracks.InputTracks2 = "Rec/Track/Keyed/ForwardFastFitted_Param"
    CompTracks.LinkerLocation = "Link/Rec/Track/ForwardFastFitted"

    CompTracks.OutputFile = "TMPcompare"
    CompTracks.OutputLevel = DEBUG
    CompTracks.addTool(IdealStateCreator("IdealStateCreator"))
    CompTracks.IdealStateCreator.Extrapolator = "TrackMasterExtrapolator"
    CompTracks.IdealStateCreator.Detectors = ["VP", "UT", "FT"]

    GaudiSequencer("CheckPatSeq").Members = [
        ParameterizedKalmanC, CompTracks
    ] + GaudiSequencer("CheckPatSeq").Members


appendPostConfigAction(doIt)
