#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
from Configurables import Brunel, TrackSys, MuonMatchPerformance

checkers = []

for ttype in ("Upstream", "MuonMatchVeloUT", "ForwardFast"):

    checker = MuonMatchPerformance("Perf" + ttype)
    checker.AcceptanceCuts = True
    checker.InputTracks = "Rec/Track/Keyed/" + ttype
    checker.LinkerLocation = "Link/Rec/Track/Keyed/" + ttype

    checkers.append(checker)

TrackSys().TrackingSequence = ["TrMuonMatch"]
TrackSys().TracksToConvert = [
    'Velo', 'Upstream', 'MuonMatchVeloUT', 'ForwardFast'
]
Brunel().DataType = "Upgrade"
Brunel().Detectors = ['VP', 'UT', 'FT', 'Muon', 'Magnet', 'Tr']
Brunel().OutputType = 'NONE'
Brunel().DisableTiming = True
Brunel().MainSequence = ["ProcessPhase/Reco", "ProcessPhase/MCLinks"
                         ] + checkers
Brunel().RecoSequence = ["Decoding", "TrFast"]

from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 2

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade-baseline-FT61-digi'].run()
