###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Brunel, CondDB, LHCbApp, DDDBConf
Brunel().DataType = "Upgrade"
Brunel().OutputType = "NONE"
Brunel().RecoSequence = ["Decoding", "TrFast"]
Brunel().EvtMax = 500
Brunel().Detectors = ["VP", "UT", "FT"]

from Configurables import RecMoniConf
RecMoniConf().MoniSequence = []

from Configurables import TrackSys
TrackSys().ExpertTracking = ["simplifiedGeometry", "ParameterizedKalman"]
TrackSys().TrackingSequence = ["TrFast"]

from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 5

from PRConfig.TestFileDB import test_file_db
test_file_db["upgrade-baseline-FT64-digi"].run()
