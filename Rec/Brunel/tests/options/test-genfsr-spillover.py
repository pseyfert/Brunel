###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

importOptions('$APPCONFIGOPTS/Brunel/MC-WithTruth.py')
importOptions('$APPCONFIGOPTS/Brunel/SplitRawEventOutput.4.3.py')
importOptions('$APPCONFIGOPTS/Brunel/ldst.py')

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += ["GenFSRMerge"]
seqGenFSR.Members += ["GenFSRRead"]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp
LHCbApp().DDDBtag = "dddb-20170301"
LHCbApp().CondDBtag = "sim-20180530-vc-md100"

from PRConfig import TestFileDB
TestFileDB.test_file_db['genFSR_upgrade_spillover_digi'].run()
