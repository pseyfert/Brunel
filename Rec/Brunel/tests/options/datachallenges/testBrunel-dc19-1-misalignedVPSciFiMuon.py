###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Default settings for all Brunel tests
from Brunel.Configuration import *

Brunel().DDDBtag = "dddb-20190223"
Brunel().CondDBtag = "DC19-1-VPSciFiMuonmisaligned"
Brunel().DataType = "Upgrade"
Brunel().MergeGenFSR = False
Brunel().DatasetName = "MisalignedVPSciFiMuon"
