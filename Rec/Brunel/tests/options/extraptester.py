#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Brunel.Configuration import *
Brunel().EvtMax = 1

from Configurables import ExtrapolatorTester
from Configurables import ApplicationMgr

from Configurables import Brunel
brunel = Brunel()

ApplicationMgr().TopAlg = [ExtrapolatorTester()]

from Configurables import TrackParametrizedExtrapolator
ExtrapolatorTester().Extrapolators = [TrackParametrizedExtrapolator()]

ExtrapolatorTester().ReferenceExtrapolator.MinStep = 1
ExtrapolatorTester().ReferenceExtrapolator.MaxStep = 10
ExtrapolatorTester().ReferenceExtrapolator.MaxNumSteps = 10000

from Configurables import MagneticFieldSvc
MagneticFieldSvc().ForcedSignedCurrentScaling = 1.0
