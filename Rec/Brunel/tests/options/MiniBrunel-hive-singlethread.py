###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import MiniBrunel
from PRConfig.TestFileDB import test_file_db

mbrunel = MiniBrunel()
mbrunel.EvtMax = 100
mbrunel.ThreadPoolSize = 1
mbrunel.EventSlots = 1

test_file_db['MiniBrunel_2018_MinBias_FTv2_MDF'].run(configurable=mbrunel)
