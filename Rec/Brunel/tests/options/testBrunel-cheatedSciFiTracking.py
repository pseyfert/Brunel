###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import appendPostConfigAction
from Configurables import Brunel, GaudiSequencer, CondDB
##########################

Brunel().PrintFreq = 1
Brunel().OutputType = "NONE"
Brunel().RecoSequence = ["Decoding", "TrFast", "TrBest"]
Brunel().MainSequence = [
    'ProcessPhase/Reco', 'ProcessPhase/MCLinks', 'ProcessPhase/Check'
]

Brunel().WithMC = True
Brunel().Simulation = True
Brunel().DDDBtag = "dddb-20190223"
Brunel().CondDBtag = "sim-20180530-vc-md100"
Brunel().DataType = "Upgrade"
Brunel().MCLinksSequence = ["Unpack", "Tr"]
Brunel().Detectors = ["VP", "UT", "FT"]
Brunel().FilterTrackStates = False


def FTconfig():
    from Configurables import PrCheatedSciFiTracking, PrHybridSeeding
    GaudiSequencer("RecoTrBestSeq").Members.remove(
        PrHybridSeeding("PrHybridSeedingBest"))
    GaudiSequencer("RecoTrBestSeq").Members.insert(
        2, PrCheatedSciFiTracking("PrCheatedSciFiTracking"))
    GaudiSequencer("MCLinksUnpackSeq").Members = []


appendPostConfigAction(FTconfig)
