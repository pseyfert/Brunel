###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import Gaudi.Configuration
from Configurables import Brunel

brunel = Brunel()
brunel.DataType = "Upgrade"
brunel.DisableTiming = True

# Needed to switch off Lumi event handling and pick up SIMCOND while waiting for real MDF files!
brunel.Simulation = True

from Configurables import FTRawBankDecoder
FTRawBankDecoder("createFTClusters").DecodingVersion = 2
from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade-baseline-FT61-digi'].run()
