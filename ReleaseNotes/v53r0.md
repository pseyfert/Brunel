2017-07-11 Brunel v53r0
===
This version uses projects LHCb v43r0, Lbcom v21r0, Rec v22r0, Gaudi v28r2, LCG_88
 (Root 6.08.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*

This version is a development release for 2018 data-taking and upgrade simulations

This version is released on `master` branch. The previous release on `master` branch  was `Brunel v52r3`. This release contains all the changes that were made in `Brunel v52r4` and `Brunel v52r5` (released on `2017-patches` branch, see corresponding release notes), as well as the additional changes described in this file.

## Change to compiler support
**As from this release, support for gcc49 is dropped**  
This means that C++14 is now fully supported


## Thread safety
**[MR Rec!593] Make TMVA NN in PrForward stateless**

## Code optimisations
**[MR Rec!572, Lbcom!132, LHCb!618] Rich MaPMT improvements**  

**Multiple code modernisations, see Rec v22r0 and LHCb v43r0 release notes**

## Bug fixes
**[MR Rec!644] Fixed typo in CaloPi0Monitor leading to bad photon counts** 

**[MR Rec!637] Fixed logging of lack of vertices in VeloIPResolutionMonitor**  
In case an empty set was in the TES, nothing was reported

**[MR Rec!576] Fix old RICH pixel sorting**  
Fixes an issue with the pixel sorting in the old RICH sequence, that affects the Upgrade productions only.

**[MR LHCb!755] `L0DURawBankMonitor` was resetting the labels for some histo at each event. Protection added**


## Monitoring changes
**[MR Lbcom!144] Reduce CaloCluster2MC verbosity**

