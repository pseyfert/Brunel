

2019-07-21 Brunel v60r5
===

This version uses Rec v30r5, Lbcom v30r5, LHCb v50r5, Gaudi v32r1 and LCG_96 with ROOT 6.18.00

This version is a development release for Run 3 reconstruction validation  

This version is released on `master` branch. The previous release on `master` branch  was Brunel `v60r4`.  


### New features

- Add CALOFUTURE sequence to MiniBrunel, !798, !801 (@cmarinbe)   

- Add support for new C++ functors that work a little differently to the existing LoKi ones, Rec!1541, LHCb!1961, !790 (@olupton)   


### Enhancements

- Update to latest LHCbCond 2015 and 2016 global tags, LHCb!1987 (@cattanem)   
  *  2016: LHCBCOND `cond-20181204` c.f. lhcb-conddb/LHCBCOND!27 (New Ecal calibration for 2016)  
  *  2015: LHCBCOND `cond-20190606` c.f. lhcb-conddb/LHCBCOND!30 (October 2015 calorimeter re-calibration)  

- Support SIMD/POD containers and vectorised selections in new functors, LHCb!2004, Rec!1601, !830, !837 (@olupton)   

- Support alternative SIMD abstraction layers, gaudi/Gaudi!912, LHCb!1932, Lbcom!361, Rec!1535, !789 (@jonrob)   

- Moved SIMD HLT1 sequence to SoA POD event model, LHCb!1913, Rec!1532, !771 (@sponce)   

- Add MDFIOAlg and MDFIOSvc allowing to efficiently read events from MDF files with no event selector, LHCb!1904 (@sponce)   

- RichTrackSegment - Set precision on 'angleToDirection' theta and phi values, LHCb!2023, !846 (@jonrob)   

- RichDetailedTrSegMakerFromTracks : Minor overhaul to improve CPU performance, Rec!1609, !839 (@jonrob)  

- RICH - Optimisations of QuarticSolverNewton, LHCb!1983, Rec!1583, Rec!1573, Rec!1566, !821, !812 (@jonrob)   

- RICH - Small optimisation of likelihood log(exp(x)-1) function., Rec!1567, !816 (@jonrob)   

- Misc. optimisations to SIMDQuarticPhotonReco, Rec!1626, LHCb!2020 (@jonrob)   

- Rich DetectablePhotonYields - Better tuned vector reserve size, other minor optimisations., !1574 (@jonrob)   

- MagneticFieldGrid::fieldVector - Small optimisation, LHCb!2002, !829 (@jonrob)   

- DetDesc - 'Solids' classes cleanup, LHCb!2005, !834 (@jonrob)   

- Switch VeloClusterTrackingSIMD to avx256, improve hit removal, Rec!1627, LHCb!2031 (@ahennequ)   

- TrackSTEPExtrapolator - Modest CPU optimisation, !1597 (@jonrob)   


### Thread safety

- Use DataHandles in unpackers, LHCb!1931, !817 (@apearce)   

- Workaround for caching in TrackStateProvider, Rec!1584 (@rmatev) [LHCBPS-1835]  



### Bug fixes

- Improvements on nightly tests, !826, !832 (@sponce)   
  Mainly 2 :  
  *  Fixed utconverter for IP cut. This will recreate proper UT efficiencies in the nightlies for Cut tests (no one noticed but they have been screwed)  
  *  Integrated fitting to Full_HLT1 test  

- Overwrite runTest functions in test inheriting from others to fix throughput tests, !824 (@ahennequ)   
  The folowing tests were run with wrong configuration when launched from throughput tests:  
    * CutNoFit  
    * CutWithFit-MasterFitter  
    * CutWithFit-VectorFitter  
    * NoCutNoFit-BestPhysics  

- Fix propagation of uncertainties in MuonMatch, Rec!1562, !808, !811 (@mramospe)   

- Divide chi2 by ndof in VeloKalman, Rec!1600 (@ahennequ)   



### Code modernisations and cleanups

- Rich : Use MsgCounter instances, LHCb!2018, Lbcom!367, Rec!1618, !841 (@jonrob)   

- Remove AddVeloInfo, Rec!1613, !840 (@cattanem)   

- Remove HltMonitors and empty Hlt monitoring sequence, Rec!1604, !838 (@cattanem)   

- Modernize CaloFuture LHCb!1990, Rec!1577, Rec!1585, !823, !828 (@graven)   
  
- Calo: First round of context removal, Rec!1556, Rec!1569, !810, !820 (@jmarchan, @graven)   

- Cleanup old EvtStoreSvc hooks, !800 (@graven)   

- Remove SPD/PRS from CaloFuture configuration, Rec!1542, !791 (@cmarinbe)   

- Remove Run1 and Run2 support from RecConf, TrackSys, Brunel Configurables, Rec!1536, Rec!1557, !784, !807 (@cattanem)   

- Remove Run1/2 Calo sequences from GlobalReco Configuration, Rec!1615 (@cattanem)   


### Monitoring changes

- Template PrTrackAssociator to support different track types, Rec!1632, !849 (@sstahl)

- Print counters in Gaudi::Algorithm::finalize gaudi/Gaudi!943, gaudi/Gaudi!946, gaudi/Gaudi!947, !844 (@graven, @clemenci, @cattanem)   
  
- Add PrTrackChecker dedicated to testing GhostProbability, Rec!1607, !836 (@sstahl)   

- Add the flag fromPV to PrChecker, Rec!1617 (@mengzhen)   

- Update references to follow new TrackResChecker histo in Rec!1600, !833 (@cattanem)   
  
- Added IPCHI2 to IPResolutionCheckerNT, Rec!1561, !809 (@ldufour)   

- Remove cuts on reconstructed quantities in PrTrackChecker, Rec!1551, !802 (@sstahl)   

- Adapt minibrunel configuration to changed mc sequence, Rec!1551, !799 (@sstahl)   

- Add algorithm name to warning in HiveDataBroker, gaudi/Gaudi!923, !851 (@sstahl)


### Changes to tests

- Added Full HLT1 FTv6 MDF test, !843 (@mvesteri)

- Fix splitting of input files in Hlt2Reconstruction, !845 (@mstahl)

- Accept "INFO Number of counters" as counters table signature, LHCb!2032 (@clemenci)   

- Added a way to specify per counter/histo sensibility in tests, LHCb!1995 (@sponce)   

- Add exclusion for 'Added successfully Conversion service:RootCnvSvc', !825 (@cattanem)   

- Lowered sensitivity of some counters in hive-multi-thread test, !822 (@sponce)   

- Add upgrade Hlt2 Reconstruction QMTest, !818 (@mstahl)   

- Add brunel-extraptester.qmt qmtest for TrackExtrapolators, !806 (@mlucioma)   

- Add tests for the IPResolutionCheckerNT, !804 (@ldufour)   

