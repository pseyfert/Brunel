2017-07-04 Brunel v52r5
---
This version uses projects LHCb v42r5, Lbcom v20r5, Rec v21r5, Gaudi v28r2, LCG_88
 (Root 6.08.06) and SQLDDDB v7r*, ParamFiles v8r*, FieldMap v5r*, AppConfig v3r*

This version is released on 2017-patches branch

This version is a patch release for the 2017 prompt reconstruction, Reco17

## Change to release notes format
**As from this release, the file `BrunelSys/doc/release.notes` is frozen**  
Instead, there will be a file per release in the new `ReleaseNotes` directory. e.g. this file is called `ReleaseNotes/v52r5.md`

## Changes to configuration
**[MR !229] Add possibility to disable TimingAuditor**  
New slot `Brunel().DisableTiming`, `False` by default

**[MR !216] Remove obsolete suppressions of run time Warning messages**

## Bug fixes
**[MR LHCb!730] Fixes for Git CondDB**   
- Make the code forwards & backwards compatible with gaudi/Gaudi!290 (same as MR LHCb!665)  
- Fixed configuration backward compatibility with COOL  
- Protected `debug()` messages


## Monitoring changes
**[MR Rec!622] Fix Tracking timing plots by adding missing sequences to timing list**

**[MR Rec!623] ProtoParticle Moni : Fix the conversion of the track type enum to string**

**[MR LHCb!731] Print out used CondDB tags even if global output level is not INFO**


## Changes to tests
**[MR !229] Disable TimingAuditor in all QM tests**  
Also modified all tests to include the options file `testBrunel-defaults.py`

**[MR !222] Remove obsolete exclusions from stdout comparisons of tests**

**[MR !218, !220, !221] Update tests following move to Git CondDB**
